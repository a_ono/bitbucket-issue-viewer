$(document).ready(function() {
  var link = function(label, url) {
    return $("<a/>").text(label).click(function() {
      air.navigateToURL(new air.URLRequest(url));
    });
  }

  var record = function(repos, issue) {
    var reposLink = link(repos.split("/")[1], bitbucket.issueUrl(repos));
    var title = link(issue.title, bitbucket.issueUrl(repos,issue.local_id));
    return $("<tr/>").append(
      $("<td/>").append(reposLink)
    ).append(
      $("<td/>").append(title)
    ).append(
      $("<td/>").append(issue.status)
    ).append(
      $("<td/>").append(issue.responsible ? issue.responsible.username : "")
    ).append(
      $("<td/>").append(issue.utc_last_updated)
    );
  }

  var allRepos = bitbucket.shared.data.repos.split(",");
  var counter = allRepos.length;
  $.each(allRepos, function(i, repos) {
    bitbucket.getIssues(repos, function(data) {
      var tbody = $("#table tbody");
      $.each(data.issues, function(i, issue) {
        tbody.append(record(repos, issue));
      });
      if (--counter == 0) {
        var table = $("#table").dataTable({
          "sDom": "<'row'<'span4 toolbar'><'span4'f>r><'row'<'span8't>><'row'<'span4'i><'span4'p>>",
          "sPaginationType": "bootstrap"
        }).show();

        $("div.toolbar").append("filter: ").append($("#filter").show().change(
          function() {
            var v = $(this).val();
            if (v == "all") {
              table.fnFilter("", 2, false);
            } else if (v == "not_resolved") {
              table.fnFilter("^(?!resolved)", 2, true);
            } else {
              table.fnFilter(v, 2, false);
            }
          }
        ).change());
      }
    });
  });

});

