var bitbucket = {
  shared: air.SharedObject.getLocal("bitbucket", "/"),

  home: "https://bitbucket.org/",
  apiRoot: "https://api.bitbucket.org/1.0/",

  issueUrl: function(repos, id) {
    return this.home + repos + (id ? "/issue/" + id : "/issues");
  },

  get: function(url, callback, error) {
    $.ajax({
      url: url,
      beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization",
          "Basic " + bitbucket.shared.data.authorization);
      },
      success: callback,
      error: error
    });
  },

  login: function(user, pass) {
    $("#message").hide();
    bitbucket.shared.data.authorization = $.base64.encode(user + ":" + pass);
    this.getRepositories(function(data){
      var a = [];
      $.each(data, function(i, repos) {
        a.push(repos.owner + "/" + repos.name);
      });
      bitbucket.shared.data.repos = a.join(",");
      bitbucket.shared.flush();
      document.location = "main.html";
    }, function() {
      $("#message").show();
    });
  },

  logout: function() {
    bitbucket.shared.clear();
    document.location = "login.html";
  },

  getRepositories: function(callback, error) {
    this.get(bitbucket.apiRoot + "user/repositories/", callback, error);
  },

  getIssues: function(repos, callback, error) {
    this.get(bitbucket.apiRoot + "repositories/" + repos + "/issues/",
      callback, error);
  }
}
