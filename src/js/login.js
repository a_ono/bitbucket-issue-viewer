if (bitbucket.shared.data.authorization && bitbucket.shared.data.repos) {
  document.location = "main.html";
}

$(document).ready(function() {
  
  $("form").submit(function() {
    bitbucket.login($("#username").val(), $("#password").val());
    return false;
  });
});
